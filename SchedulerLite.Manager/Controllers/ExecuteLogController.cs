﻿using SchedulerLite.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchedulerLite.Manager.Controllers
{
    public class ExecuteLogController : Controller
    {
        // GET: ExecuteLog
        public ActionResult List(long taskId = 0, int taskType = 0, int status = 0, int page = 1)
        {
            var pageSize = 10;
            var plist = B_ExecuteLog.SearchList(taskId, taskType, status, page, pageSize);
            ViewBag.plist = plist.ToStaticPagedList();
            return View();
        }

    }
}