﻿using SchedulerLite.BLL;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchedulerLite.Manager.Controllers
{
    public class DelayTaskController : Controller
    {
        // GET: DelayTask
        public ActionResult List(string name, int page = 1)
        {
            var pageSize = 10;
            var plist = B_DelayTask.SearchList(name, page, pageSize);
            ViewBag.plist = plist.ToStaticPagedList();
            return View();
        }

        public ActionResult Edit(long id = 0)
        {
            var m = B_DelayTask.SingleById(id);
            if (m == null)
            {
                m = new DelayTask();
                m.Enable = true;
                m.Method = "GET";
                m.TimeoutSeconds = 15;
                m.MaxRetryCount = 3;
                m.RetrySeconds = 30;
            }
            ViewBag.m = m;
            return View();
        }
        public ActionResult DoEdit(DelayTask m)
        {
            var ro = B_DelayTask.Edit(m);
            return Json(ro);
        }

        public ActionResult DoDelete(long id)
        {
            var ro = B_DelayTask.Delete(id);
            return Json(ro);
        }

        public ActionResult NewTask(DelayTask m)
        {
            m.

            var ro = B_DelayTask.Edit(m);
            return Json(ro);
        }
    }
}