﻿using SchedulerLite.BLL;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchedulerLite.Manager.Controllers
{
    public class TimedTaskController : Controller
    {
        // GET: TimedTask
        public ActionResult List(string name, bool? enable, int page = 1)
        {
            var pageSize = 10;
            var plist = B_TimedTask.SearchList(name, enable, page, pageSize);
            ViewBag.plist = plist.ToStaticPagedList();
            return View();
        }

        public ActionResult Edit(long id = 0)
        {
            var m = B_TimedTask.SingleById(id);
            if (m == null)
            {
                m = new TimedTask();
                m.Enable = true;
                m.Method = "GET";
                m.TimeoutSeconds = 15;
            }
            ViewBag.m = m;
            return View();
        }
        public ActionResult DoEdit(TimedTask m)
        {
            var ro = B_TimedTask.Edit(m);
            return Json(ro);
        }

        public ActionResult DoDelete(long id)
        {
            var ro = B_TimedTask.Delete(id);
            return Json(ro);
        }
    }
}