
USE [SchedulerLite]
GO
/****** Object:  Table [dbo].[DelayTask]    Script Date: 2018/1/18 15:21:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DelayTask](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Url] [varchar](500) NOT NULL,
	[Method] [varchar](10) NOT NULL,
	[PostData] [varchar](max) NOT NULL,
	[TriggerTime] [datetime] NOT NULL,
	[MaxRetryCount] [int] NOT NULL,
	[RetryCount] [int] NOT NULL,
	[RetrySeconds] [int] NOT NULL,
	[ExecCount] [bigint] NOT NULL,
	[TimeoutSeconds] [int] NOT NULL,
	[SuccessFlag] [varchar](50) NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[Enable] [bit] NOT NULL,
 CONSTRAINT [PK_DelayTask] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExecuteLog]    Script Date: 2018/1/18 15:21:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExecuteLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TaskId] [bigint] NOT NULL,
	[TaskName] [varchar](100) NOT NULL,
	[TaskUrl] [varchar](500) NOT NULL,
	[TaskType] [int] NOT NULL,
	[TaskMethod] [varchar](10) NOT NULL,
	[PostData] [varchar](max) NOT NULL,
	[Status] [int] NOT NULL,
	[Message] [varchar](max) NOT NULL,
	[AddTime] [datetime] NOT NULL,
 CONSTRAINT [PK_ExecuteLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimedTask]    Script Date: 2018/1/18 15:21:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimedTask](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Cron] [varchar](200) NOT NULL,
	[Url] [varchar](500) NOT NULL,
	[Method] [varchar](10) NOT NULL,
	[PostData] [varchar](max) NOT NULL,
	[ExecCount] [bigint] NOT NULL,
	[LastExecTime] [datetime] NOT NULL,
	[LastStatus] [int] NOT NULL,
	[TimeoutSeconds] [int] NOT NULL,
	[Enable] [bit] NOT NULL,
	[AddTime] [datetime] NOT NULL,
	[UpdateTime] [datetime] NOT NULL,
	[SuccessFlag] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TimedTask] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_Url]  DEFAULT ('') FOR [Url]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_Method]  DEFAULT ('') FOR [Method]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_PostData]  DEFAULT ('') FOR [PostData]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_TriggerTime]  DEFAULT ('1900-1-1') FOR [TriggerTime]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_MaxRetryCount]  DEFAULT ((0)) FOR [MaxRetryCount]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_RetryCount]  DEFAULT ((0)) FOR [RetryCount]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_RetrySeconds]  DEFAULT ((0)) FOR [RetrySeconds]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_ExecCount]  DEFAULT ((0)) FOR [ExecCount]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_TimeoutSeconds]  DEFAULT ((0)) FOR [TimeoutSeconds]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_SuccessFlag]  DEFAULT ('') FOR [SuccessFlag]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
ALTER TABLE [dbo].[DelayTask] ADD  CONSTRAINT [DF_DelayTask_Enable]  DEFAULT ((0)) FOR [Enable]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_TaskId]  DEFAULT ((0)) FOR [TaskId]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_TaskName]  DEFAULT ('') FOR [TaskName]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_TaskUrl]  DEFAULT ('') FOR [TaskUrl]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_TaskType]  DEFAULT ((0)) FOR [TaskType]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_TaskMethod]  DEFAULT ('') FOR [TaskMethod]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_PostData]  DEFAULT ('') FOR [PostData]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_Success]  DEFAULT ((0)) FOR [Status]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_Message]  DEFAULT ('') FOR [Message]
GO
ALTER TABLE [dbo].[ExecuteLog] ADD  CONSTRAINT [DF_ExecuteLog_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_Name]  DEFAULT ('') FOR [Name]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_Cron]  DEFAULT ('') FOR [Cron]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_Url]  DEFAULT ('') FOR [Url]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_Method]  DEFAULT ('') FOR [Method]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_FormData]  DEFAULT ('') FOR [PostData]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_ExecCount]  DEFAULT ((0)) FOR [ExecCount]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_LastExecTime]  DEFAULT ('1900-1-1') FOR [LastExecTime]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_LastStatus]  DEFAULT ((0)) FOR [LastStatus]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_TimeoutSeconds]  DEFAULT ((0)) FOR [TimeoutSeconds]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_Enable]  DEFAULT ((0)) FOR [Enable]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_AddTime]  DEFAULT (getdate()) FOR [AddTime]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_UpdateTime]  DEFAULT (getdate()) FOR [UpdateTime]
GO
ALTER TABLE [dbo].[TimedTask] ADD  CONSTRAINT [DF_TimedTask_SuccessFlag]  DEFAULT ('') FOR [SuccessFlag]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'请求地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'Url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POST/GET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'Method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'post数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'PostData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'触发时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'TriggerTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最大重试次数，0无限，-1不重试' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'MaxRetryCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'重试次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'RetryCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'不成功时，重试间隔秒数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'RetrySeconds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'ExecCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'超时秒数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'TimeoutSeconds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成功标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'SuccessFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'AddTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask', @level2type=N'COLUMN',@level2name=N'Enable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'延迟任务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'DelayTask'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'日志名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'Id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'TaskId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'TaskName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'TaskUrl'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1-定时任务，2-延迟任务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'TaskType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'方法' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'TaskMethod'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'投递的数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'PostData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1成功，2失败，3超时' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'Status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'信息' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'Message'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog', @level2type=N'COLUMN',@level2name=N'AddTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行日志' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ExecuteLog'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'cron表达式' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'Cron'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'任务地址' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'Url'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POST/GET' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'Method'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'POST数据' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'PostData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'执行次数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'ExecCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后执行时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'LastExecTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最后执行的状态1成功，2失败，3超时' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'LastStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'超时秒数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'TimeoutSeconds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否启用' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'Enable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'添加时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'AddTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'修改时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'UpdateTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'成功标识' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask', @level2type=N'COLUMN',@level2name=N'SuccessFlag'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'定时任务' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'TimedTask'
GO
USE [master]
GO
ALTER DATABASE [SchedulerLite] SET  READ_WRITE 
GO
