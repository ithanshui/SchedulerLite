﻿using Loogn.OrmLite;
using SchedulerLite.DAL;
using SchedulerLite.Model;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulerLite.BLL
{
    public class B_DelayTask
    {

        public static ResultObject Edit(DelayTask m)
        {
            if (string.IsNullOrEmpty(m.Url))
            {
                return new ResultObject("url不能为空");
            }
            if (m.Id > 0)
            {

                var flag = D_DelayTask.Update(m);
                return new ResultObject(flag);
            }
            else
            {
                if (m.TriggerTime <= DateTime.Now)
                {
                    return new ResultObject("触发时间不正确");
                }
                var flag = D_DelayTask.Add(m);
                return new ResultObject(flag);
            }
        }

        public static OrmLitePageResult<DelayTask> SearchList(string name, int pageIndex, int pageSize)
        {
            return D_DelayTask.SearchList(name, pageIndex, pageSize);
        }

        public static List<DelayTask> SelectReadyList(DateTime beginTime, DateTime endTime)
        {
            return D_DelayTask.SelectReadyList(beginTime, endTime);
        }
        public static List<DelayTask> SelectByIds(List<long> ids)
        {
            return D_DelayTask.SelectByIds(ids);
        }

        public static DelayTask SingleById(long id)
        {
            return D_DelayTask.SingleById(id);
        }

        public static ResultObject Delete(long id)
        {
            var flag = D_DelayTask.Delete(id);
            return new ResultObject(flag);
        }

        public static bool Retry(DelayTask task)
        {
            if (task.RetryCount >= task.MaxRetryCount)
            {
                return false;
            }
            task.TriggerTime = DateTime.Now.AddSeconds(task.RetrySeconds);
            D_DelayTask.RetryUpdate(task);
            return true;
        }
    }
}
