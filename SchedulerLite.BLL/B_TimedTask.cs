﻿using Loogn.OrmLite;
using SchedulerLite.Common.Quartz;
using SchedulerLite.DAL;
using SchedulerLite.Model;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulerLite.BLL
{
    public class B_TimedTask
    {
        public static ResultObject Edit(TimedTask m)
        {
            if (string.IsNullOrEmpty(m.Url))
            {
                return new ResultObject("url不能为空");
            }
            if (string.IsNullOrEmpty(m.Cron))
            {
                return new ResultObject("cron表达不能为空");
            }
            try
            {
                var exp = new CronExpression(m.Cron);
            }
            catch
            {
                return new ResultObject("cron表达式不正确");
            }

            if (m.Id > 0)
            {
                m.UpdateTime = DateTime.Now;
                var flag = D_TimedTask.Update(m);
                return new ResultObject(flag);
            }
            else
            {
                m.LastExecTime = new DateTime(1900, 1, 1);
                var flag = D_TimedTask.Add(m);
                return new ResultObject(flag);
            }
        }

        public static OrmLitePageResult<TimedTask> SearchList(string name, bool? enable, int pageIndex, int pageSize)
        {
            return D_TimedTask.SearchList(name, enable, pageIndex, pageSize);
        }

        public static TimedTask SingleById(long id)
        {
            return D_TimedTask.SingleById(id);
        }

        public static List<TimedTask> SelectByIds(List<long> ids)
        {
            return D_TimedTask.SelectByIds(ids);
        }


        public static ResultObject Delete(long id)
        {
            var flag = D_TimedTask.Delete(id);
            return new ResultObject(flag);
        }

        public static Dictionary<string, List<long>> SelectReadyList()
        {
            return D_TimedTask.SelectReadyList();
        }

        public static int ExecuteOne(TimedTask task)
        {
            task.LastExecTime = DateTime.Now;
            return D_TimedTask.ExecuteOne(task);
        }
    }
}
