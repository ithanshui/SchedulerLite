﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SchedulerLite.Service
{
    class TaskTimer
    {
        private DelayTaskManager _delayTaskManager;
        private TimedTaskManager _timedTaskManager;
        private Timer _timer;

        public TaskTimer(DelayTaskManager delayTaskManager, TimedTaskManager timedTaskManager)
        {
            _delayTaskManager = delayTaskManager;
            _timedTaskManager = timedTaskManager;
            _timer = new Timer(1000);
            _timer.Elapsed += _timer_Elapsed;
        }
        public void Start()
        {
            _timer.Start();
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            var now = DateTime.Now;//触发的时间
            Task.Factory.StartNew(() =>
            {
                Task.Factory.StartNew(() => { _timedTaskManager.LoadTask(now); }).ContinueWith(task =>
                {
                    _timedTaskManager.TriggerTask(now);
                });

                Task.Factory.StartNew(() => { _delayTaskManager.LoadTask(now); }).ContinueWith(task =>
                {
                    _delayTaskManager.TriggerTask(now);
                });
            });
        }

    }
}
