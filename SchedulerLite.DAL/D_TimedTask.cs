﻿using Loogn.OrmLite;
using SchedulerLite.Model.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulerLite.DAL
{
    public static class D_TimedTask
    {
        public static long Add(TimedTask m)
        {
            using (var db = DB.Open())
            {
                return db.Insert(m);
            }
        }

        public static long Update(TimedTask m)
        {
            using (var db = DB.Open())
            {
                return db.Update(m, "Name", "Cron", "Url", "Method", "PostData", "Enable", "UpdateTime", "SuccessFlag", "TimeoutSeconds");
            }
        }

        public static OrmLitePageResult<TimedTask> SearchList(string name, bool? enable, int pageIndex, int pageSize)
        {
            var condition = "1=1";
            name = SqlInjection.Filter(name);
            if (!string.IsNullOrEmpty(name))
            {
                condition += " and Name like '%" + name + "%'";
            }
            if (enable != null)
            {
                condition += " and Enable=" + (enable.Value ? "1" : "0");
            }

            using (var db = DB.Open())
            {
                return db.SelectPage<TimedTask>(new OrmLitePageFactor
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    OrderBy = "id desc",
                    Conditions = condition,
                });
            }
        }

        public static TimedTask SingleById(long id)
        {
            using (var db = DB.Open())
            {
                return db.SingleById<TimedTask>(id);
            }
        }

        public static List<TimedTask> SelectByIds(List<long> ids)
        {
            using (var db = DB.Open())
            {
                return db.SelectByIds<TimedTask>(ids);
            }
        }

        public static int Delete(long id)
        {
            using (var db = DB.Open())
            {
                return db.DeleteById<TimedTask>(id);
            }
        }

        public static Dictionary<string, List<long>> SelectReadyList()
        {
            using (var db = DB.Open())
            {
                var sql = "SELECT Cron,Id FROM TimedTask WHERE Enable=1";
                return db.Lookup<string, long>(sql);
            }
        }

        public static int ExecuteOne(TimedTask task)
        {
            using (var db = DB.Open())
            {
                return db.UpdateById<TimedTask>(
                    DictBuilder.Assign("LastExecTime", task.LastExecTime)
                    .Assign("LastStatus", task.LastStatus)
                    .Assign("$ExecCount", "ExecCount+1"), task.Id);
            }
        }
    }
}
