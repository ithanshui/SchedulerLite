﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchedulerLite.Model
{
    public class RunTaskInfo
    {
        /// <summary>
        /// 任务编号
        /// </summary>
        public long Id { get; set; }
        /// <summary>
        /// 任务名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 请求地址
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// POST / GET
        /// </summary>
        public string Method { get; set; }
        /// <summary>
        /// POST数据
        /// </summary>
        public string PostData { get; set; }
        public int TimeoutSeconds { get; set; }
    }
}
